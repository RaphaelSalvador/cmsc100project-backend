/**
 * This is the test function
 * 
 * @param {*} request 
 * @param {*} reply 
 * @returns {*}
 */

 export async function test (request, reply) {
    return {
        hello: 'world from test'
    }
};